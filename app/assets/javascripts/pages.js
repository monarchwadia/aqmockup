// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
//= require_tree ./components

var MessagingSystem = {};

(function(){
    var bindings = [];
    MessagingSystem.bindings = bindings;

    function addRoute(key, callback){
        bindings[key] = callback;
    }

    function send(key, data) {
        bindings[key](data);
    }

    MessagingSystem.addRoute = addRoute;
    MessagingSystem.send = send;
})();

React.render(
    <MainBody />,
    document.getElementById('content')
);