var MainBody = React.createClass({
    listAllColumns : function() {
        var campaignColumns = [
            {
                headerTitle: "ID",
                propertyKey: "id"
            },
            {
                headerTitle: "Name",
                propertyKey: "name"
            },
            {
                headerTitle: "Advertiser",
                propertyKey: "advertiser"
            },
            {
                headerTitle: "Email",
                propertyKey: "email"
            },
            {
                headerTitle: "Click URL",
                propertyKey: "click_url"
            },
            {
                headerTitle: "Resolved URL",
                propertyKey: "resolved_url"
            },
            {
                headerTitle: "Review",
                propertyKey: "review"
            },
            {
                headerTitle: "Attributes",
                propertyKey: "attributes"
            },
            {
                headerTitle: "Start Date",
                propertyKey: "start_date"
            },
            {
                headerTitle: "Daily Budget",
                propertyKey: "daily_budget"
            },
            {
                headerTitle: "Lifetime Budget",
                propertyKey: "lifetime_budget"
            },
            {
                headerTitle: "Time",
                propertyKey: "time"
            },
            {
                headerTitle: "Last Edited",
                propertyKey: "last_edited"
            },
            {
                headerTitle: "Last Edited By",
                propertyKey: "last_edited_by"
            }
        ];
        return campaignColumns;
    },
    getFakeData: function() {
        var returnObject = [];
        for(var i=1; i<100; i++) {
            var advertiserName = chance.word({syllables: 3});
            returnObject.push({
                id: i,
                name: chance.name(),
                advertiser: advertiserName,
                email: chance.email(),
                click_url: chance.url(),
                resolved_url: chance.url(),
                review: chance.bool().toString(),
                attributes: "Attributes",
                start_date: chance.date({year:2014}).toLocaleDateString(),
                daily_budget: "$" + chance.integer({min: 0, max: 1000}),
                lifetime_budget: "$" + chance.integer({min: 5000, max: 500000}),
                time: chance.hour({twentyfour: true}) + "h" + chance.minute(),
                last_edited: chance.date({year:2015}).toLocaleDateString(),
                last_edited_by: chance.first() + " " + chance.character({casing: 'upper'}),
                creativeUrl: "https://placehold.it/350x150?text=" + advertiserName
            })
        }
        return returnObject;
    },
    getInitialState : function() {
        return {
            data: this.getFakeData(),
            columns: this.listAllColumns(),
            selectedId: 1,
            hoveredId: 1
        };
    },
    setSelectedId: function(newId) {
        this.setState({selectedId: newId});
    },
    setHoveredId: function(newId) {
        this.setState({hoveredId: newId});
    },
    componentWillMount : function() {
        MessagingSystem.addRoute("setHoveredId", this.setHoveredId);
        MessagingSystem.addRoute("setSelectedId", this.setSelectedId);
    },
    render: function() {
        return (
            <div className="mainBody fill" >
                <Campaigns data={this.state} />
                <CampaignDetails data={this.state} />
            </div>
        )
    }
});
