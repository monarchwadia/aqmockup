var CampaignDetails = React.createClass({
    render: function() {
        return (
            <div className="detailsBox">
                <div className="col-xs-6">
                    <img src={this.props.data.data[this.props.data.selectedId -1].creativeUrl} />
                </div>
                <div className="col-xs-6">{JSON.stringify(this.props.data.data[this.props.data.selectedId -1])}</div>
            </div>
        )
    }
});