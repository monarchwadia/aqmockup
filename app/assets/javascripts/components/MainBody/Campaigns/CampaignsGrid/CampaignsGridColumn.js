var CampaignsGridColumn = React.createClass({
    render: function() {
        var theData = this.props.data;
        var propertyKey = this.props.propertyKey;
        var listItemNodes = this.props.data.data.map(function(listItem){
            return (
                <CampaignsGridColumnItem itemId={listItem["id"]} propertyValue={listItem[propertyKey]} data={theData} />
            )
        });
        return (
            <div className="campaignsGridColumn">
                <div className="columnHeader">{this.props.headerTitle}</div>
                <div className="columnBody">
                    {listItemNodes}
                </div>
            </div>
        )
    }
})