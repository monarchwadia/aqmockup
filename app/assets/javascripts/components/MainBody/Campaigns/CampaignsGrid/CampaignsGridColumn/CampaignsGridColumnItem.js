var CampaignsGridColumnItem = React.createClass({
    onMouseEnterHandler: function(e) {
        MessagingSystem.send("setHoveredId", this.props.itemId);
    },
    onClickHandler: function(e) {
        MessagingSystem.send("setSelectedId", this.props.itemId);
    },
    render: function() {
        var classes = React.addons.classSet ({
            'campaignsGridColumnItem': true,
            'selected': this.props.itemId == this.props.data.selectedId,
            'hovered': this.props.itemId == this.props.data.hoveredId
        });
        return (
            <div className={classes} onMouseEnter={this.onMouseEnterHandler} onClick={this.onClickHandler} >
                {this.props.propertyValue}
            </div>
        )
    }
});