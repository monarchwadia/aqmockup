var CampaignsGrid = React.createClass({
    render: function() {
        var theData = this.props.data;
        var columnList = this.props.data.columns;
        var campaignColumnNodes = columnList.map(function(column) {
            return (
                <CampaignsGridColumn headerTitle={column.headerTitle} propertyKey={column.propertyKey} data={theData} />
            )
        });

        return (
            <div className="campaignsGrid">
                {campaignColumnNodes}
            </div>
        )
    }
});