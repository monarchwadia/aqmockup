var Campaigns = React.createClass({
    render: function() {
        return (
            <div className="campaigns col-xs-12 half">
                <CampaignsFilterBar data={this.props.data} />
                <CampaignsGrid data={this.props.data} />
            </div>
        )
    }
});