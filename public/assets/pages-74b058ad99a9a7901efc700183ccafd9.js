var MainBody = React.createClass({
    listAllColumns : function() {
        var campaignColumns = [
            {
                headerTitle: "ID",
                propertyKey: "id"
            },
            {
                headerTitle: "Name",
                propertyKey: "name"
            },
            {
                headerTitle: "Advertiser",
                propertyKey: "advertiser"
            },
            {
                headerTitle: "Email",
                propertyKey: "email"
            },
            {
                headerTitle: "Click URL",
                propertyKey: "click_url"
            },
            {
                headerTitle: "Resolved URL",
                propertyKey: "resolved_url"
            },
            {
                headerTitle: "Review",
                propertyKey: "review"
            },
            {
                headerTitle: "Attributes",
                propertyKey: "attributes"
            },
            {
                headerTitle: "Start Date",
                propertyKey: "start_date"
            },
            {
                headerTitle: "Daily Budget",
                propertyKey: "daily_budget"
            },
            {
                headerTitle: "Lifetime Budget",
                propertyKey: "lifetime_budget"
            },
            {
                headerTitle: "Time",
                propertyKey: "time"
            },
            {
                headerTitle: "Last Edited",
                propertyKey: "last_edited"
            },
            {
                headerTitle: "Last Edited By",
                propertyKey: "last_edited_by"
            }
        ];
        return campaignColumns;
    },
    getFakeData: function() {
        var returnObject = [];
        for(var i=1; i<100; i++) {
            var advertiserName = chance.word({syllables: 3});
            returnObject.push({
                id: i,
                name: chance.name(),
                advertiser: advertiserName,
                email: chance.email(),
                click_url: chance.url(),
                resolved_url: chance.url(),
                review: chance.bool().toString(),
                attributes: "Attributes",
                start_date: chance.date({year:2014}).toLocaleDateString(),
                daily_budget: "$" + chance.integer({min: 0, max: 1000}),
                lifetime_budget: "$" + chance.integer({min: 5000, max: 500000}),
                time: chance.hour({twentyfour: true}) + "h" + chance.minute(),
                last_edited: chance.date({year:2015}).toLocaleDateString(),
                last_edited_by: chance.first() + " " + chance.character({casing: 'upper'}),
                creativeUrl: "https://placehold.it/350x150?text=" + advertiserName
            })
        }
        return returnObject;
    },
    getInitialState : function() {
        return {
            data: this.getFakeData(),
            columns: this.listAllColumns(),
            selectedId: 1,
            hoveredId: 1
        };
    },
    setSelectedId: function(newId) {
        this.setState({selectedId: newId});
    },
    setHoveredId: function(newId) {
        this.setState({hoveredId: newId});
    },
    componentWillMount : function() {
        MessagingSystem.addRoute("setHoveredId", this.setHoveredId);
        MessagingSystem.addRoute("setSelectedId", this.setSelectedId);
    },
    render: function() {
        return (
            <div className="mainBody fill" >
                <Campaigns data={this.state} />
                <CampaignDetails data={this.state} />
            </div>
        )
    }
});
var CampaignDetails = React.createClass({
    render: function() {
        return (
            <div className="detailsBox">
                <div className="col-xs-6">
                    <img src={this.props.data.data[this.props.data.selectedId -1].creativeUrl} />
                </div>
                <div className="col-xs-6">{JSON.stringify(this.props.data.data[this.props.data.selectedId -1])}</div>
            </div>
        )
    }
});
var Campaigns = React.createClass({
    render: function() {
        return (
            <div className="campaigns col-xs-12 half">
                <CampaignsFilterBar data={this.props.data} />
                <CampaignsGrid data={this.props.data} />
            </div>
        )
    }
});
var CampaignsFilterBar = React.createClass({
    render: function() {
        return (
            <div className="campaignsGridFilterBar">
                <input type="text" /> {this.props.data.selectedId}
            </div>
        )
    }
});
var CampaignsGrid = React.createClass({
    render: function() {
        var theData = this.props.data;
        var columnList = this.props.data.columns;
        var campaignColumnNodes = columnList.map(function(column) {
            return (
                <CampaignsGridColumn headerTitle={column.headerTitle} propertyKey={column.propertyKey} data={theData} />
            )
        });

        return (
            <div className="campaignsGrid">
                {campaignColumnNodes}
            </div>
        )
    }
});
var CampaignsGridColumn = React.createClass({
    render: function() {
        var theData = this.props.data;
        var propertyKey = this.props.propertyKey;
        var listItemNodes = this.props.data.data.map(function(listItem){
            return (
                <CampaignsGridColumnItem itemId={listItem["id"]} propertyValue={listItem[propertyKey]} data={theData} />
            )
        });
        return (
            <div className="campaignsGridColumn">
                <div className="columnHeader">{this.props.headerTitle}</div>
                <div className="columnBody">
                    {listItemNodes}
                </div>
            </div>
        )
    }
})
;
var CampaignsGridColumnItem = React.createClass({
    onMouseEnterHandler: function(e) {
        MessagingSystem.send("setHoveredId", this.props.itemId);
    },
    onClickHandler: function(e) {
        MessagingSystem.send("setSelectedId", this.props.itemId);
    },
    render: function() {
        var classes = React.addons.classSet ({
            'campaignsGridColumnItem': true,
            'selected': this.props.itemId == this.props.data.selectedId,
            'hovered': this.props.itemId == this.props.data.hoveredId
        });
        return (
            <div className={classes} onMouseEnter={this.onMouseEnterHandler} onClick={this.onClickHandler} >
                {this.props.propertyValue}
            </div>
        )
    }
});
// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.


var MessagingSystem = {};

(function(){
    var bindings = [];
    MessagingSystem.bindings = bindings;

    function addRoute(key, callback){
        bindings[key] = callback;
    }

    function send(key, data) {
        bindings[key](data);
    }

    MessagingSystem.addRoute = addRoute;
    MessagingSystem.send = send;
})();

React.render(
    <MainBody />,
    document.getElementById('content')
);
